package aipRequest;

import static org.junit.Assert.*;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import restAssuredFramework.RestAssuredExtension;

public class APITestingAndAutomation {
	private String token = TokenSingleton.getInstance();
	private static String id;
	@Test
	public void getJobs() {

		Response resp1 = RestAssured.given().headers("Authorization", "Bearer " + token).contentType(ContentType.JSON)
				.get("http://192.168.10.30:8383/job");

		System.out.println("Code" + resp1.getStatusCode());
		System.out.println("Data" + resp1.asString());
		System.out.println("Time" + resp1.getTime());
	}
		@Test
		public void addJobs() {	
		JSONObject json1 = new JSONObject();
		json1.put("createdAt", "");
		json1.put("createdBy", "");
		json1.put("designation", "New Job");
		json1.put("id", "");
		json1.put("name", "Job");
		json1.put("updatedAt", "");
		json1.put("updatedBy", "");
		Response resp2 = RestAssured.given().header("Content-Type", "application/json").body(json1)
				.post("http://192.168.10.30:8383/job");
		 id = resp2.jsonPath().getString("id");
		System.out.println("Code" + resp2.getStatusCode());
		System.out.println("Data" + resp2.asString());
		System.out.println("Time" + resp2.getTime());	
		}
		
		@Test
		public void deleteJobs() {
		Response resp3 = RestAssured.given().header("Content-Type", "application/json")
				.delete("http://192.168.10.30:8383/job/" + id);
		System.out.println("Code" + resp3.getStatusCode());
		System.out.println("Time" + resp3.getTime());
	}
}