package aipRequest;

import org.json.simple.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TokenSingleton {

	private static String token;

	private TokenSingleton() {

	}

	public static String getInstance() {
		if (token == null) {
			JSONObject json = new JSONObject();
			json.put("login", "admin@aymax.fr");
			json.put("password", "aymax2019");

			Response resp = RestAssured.given().header("Content-Type", "application/json").body(json)
					.post("http://192.168.10.30:8761/api/authenticate");
			String result = resp.getBody().jsonPath().prettify();
			System.out.println(result);

			token = resp.jsonPath().getString("id_token");
			System.out.println(token);
		}

		return token;
	}

}
